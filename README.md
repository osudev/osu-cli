[中文说明请点击此处](#markdown-header-readme-zh)

# osu!cli Beatmap Downloader

osu!cli is a tool to help you subscribe to osu! mappers and download their beatmaps through mirrors and the official website.

You can specify a list of mappers' usernames. The program will grab the list of all his/her beatmaps, then it will iterate through mirror websites (Bloodcat, loli.ol, osu.uu.gl, and old.ppy.sh) to find the first available link and then download it.

You can also specify your `Songs` where osu stores all your beatmaps. The program will check if the beatmap exists in the folder, and skip it if it does. You also have the option to toggle duplication check, and write your own filter in the code.

Usage
--------

You can follow the instructions below or watch the [walk-through](https://www.youtube.com/watch?v=IGf8XdN5dB4).

1. Download and extract the [latest release](https://bitbucket.org/osudev/osu-cli/get/master.zip), or clone with git
2. Run `start.cmd` in the extracted folder and follow the instructions

TODOs
--------

- [x] i18n.
- [x] When an unavailable beatmap set is seen, add it into a list stored on the disk. In the next time the list is used to filter out the candidates. A switch in the config toggles this feature.
- [x] Use a more elegant progress bar.
- [ ] Add an auto update functionality.
- [ ] Implement download resume functionality.
- [ ] Implement parallel downloads.
- [ ] Implement speed limitation.
- [ ] Refactor the download services into modular components.
- [ ] Replace async with promise.
- [ ] Create a GUI wrapper.

# README-zh

## 简介
osu!cli 是一款帮助你订阅你喜爱的Mapper，自动从官方或镜像站下载最新Beatmaps的自动化脚本程序。

## 下载链接
https://bitbucket.org/osudev/osu-cli/get/master.zip

## 超详细食用步骤
1. 解压下载的zip文件，打开解压后的文件夹，直到你看到有一堆文件为止
2. 找到并运行start.cmd文件，注意这不是一个exe，图标不太一样
3. 第一次运行需要进行一些初始化设置（3-15步都是）。首先程序会提示你安装Node.js，输入y然后按回车即可自动下载安装。演示视频里几秒钟装完是因为我网速快，所以有些用户可能会等得久一点，这是正常现象
4. 程序要求你申请一枚osu!开放接口的认证码（API），问你要不要打开网页，你输入y然后回车就可以跳去osu!官网的申请页面了
5. 注册API用的程序名称和URL可以随便填写，可以像我演示视频中那样直接写osu!cli和http://example.com
6. 得到API Key一枚，复制下来，回到osu!cli的窗口，右键粘贴，回车
7. 问你要不要启用osu!官网的下图服务。如果你不启用则输入n，那么程序就不会询问你的osu!账号登录信息。如果你输入y启用，那么就需要设置你的osu!用户名密码
8. 视频中上一步输入y回车，所以在这一步要输入登录信息
9. 设置osu!游戏安装目录：右键你的osu!游戏图标，打开文件所在目录，把地址栏的文字复制下来，粘贴到程序里
10. 设置Beatmap的下载目录，同样，打开下载的文件夹，把地址栏的文字复制下来，粘贴到程序里
11. 接下来会问你要不要编辑Mapper订阅列表文件，建议输入y立刻进行编辑
12. 由于你是第一次使用编辑功能，程序会问你要不要设置一个默认编辑器，输入y跳去设置页面
13. 在编辑器设置菜单，如果你的网速慢或者不想下载多余的东西，可以选择输入1回车默认使用记事本进行文件编辑。但是有网速的孩纸还是建议选用第二个：输入2回车
14. 程序会打开Mapper订阅列表文件。你看到的是默认的列表，就是我（程序作者）比较中意的Mappers，如果你不介意网络爆满的话可以留着，不然可以清空掉自己写。一行写一个Mapper的用户名，不区分大小写。写完保存退出编辑器
15. 自动下载程序需要的模块。这一步在视频中也是秒传的，但是有些人的网络就不一定了。。
16. 然后就会自动跳到程序主菜单了。以后每次运行start.cmd的时候以上所有步骤都不会再出现了！！
17. 如果打算立刻下图，那么可以输入1回车，或者直接按回车默认就是选择1
18. 接下来就是全程网速杀的下载过程啦！～么么哒！～

## 系统要求
Windows 7、Windows 8、Windows 8.1

**注：**Windows 7以下的系统版本未经测试，可能需要安装PowerShell：http://go.microsoft.com/fwlink/?LinkId=293881