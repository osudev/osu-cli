Locale = zh-TW


# Main Menu

MainMenuTitle = 歡迎使用 osu!cli 下圖工具！
MainMenuStartDownload = 開始下載
MainMenuEditConfig = 編輯配置文件
MainMenuEditUserList = 編輯Mapper列表文件
MainMenuOpenDownloadFolder = 打開下載目錄
MainMenuSetupEditor = 設置默認文本編輯器


# Constants

ConstConsoleTitle = osu!cli 下圖工具


# Configs

ConfigsInitializingSettings = 正在進行初始化設置...
ConfigsApiRequired = 本程式需要設置osu!開放接口認證碼。
ConfigsApiApply = 你可以訪問 https://old.ppy.sh/p/api 申請一枚。\nApplication Name和URL可以隨便填寫。\n申請認證碼不會對你的賬號造成任何影響，請放心操作。
ConfigsApiWebPrompt = 你想現在打開認證碼申請頁面嗎?
ConfigsApiPrompt = 復制網頁上的開放接口認證碼（API Key），右鍵本窗口進行粘貼，然後按回車
ConfigsOsuRequired = osu!官方的下載服務需要進行用戶驗證，你需要提供你的osu!用戶名和密碼。
ConfigsOsuExplain = 如果不啟用該下載服務，則不需要提供登錄信息。如果啟用，本程式則會從配置文件中讀取你的登錄信息。你可以在本初始化引導過程中填寫這些項目，但是你的密碼將以明文的方式儲存。如果你覺得不妥，可以留空這些項目，讓程式在每次執行的時候詢問你進行輸入。
ConfigsOsuPrompt = 你想啟用osu!官方的下載服務嗎?
ConfigsOsuLoginTitle = 請輸入你的osu!登錄信息
ConfigsOsuLoginUsername = 用戶名
ConfigsOsuLoginPassword = 密碼 （隱藏輸入）
ConfigsOsuDirExplain = 如果你提供osu!的安裝目錄，下載前程式會自動跳過已經存在的圖。
ConfigsOsuDirExample = 例如： D:\\osu!
ConfigsOsuDirPrompt = osu!安裝目錄
ConfigsDownloadDirExplain = 請指定一個目錄用來保存下好的圖。你可以將這個目錄設置成你osu!的Songs目錄，以便讓下好的圖自動添加到曲庫，但是這樣做有可能會與osu!主程式產生沖突，最好的方式還是使用一個獨立的目錄進行儲存，等下載結束後再導入遊戲中。
ConfigsDownloadDirExample = 例如： E:\\Downloads\\Beatmaps
ConfigsDownloadDirPrompt = 下載目錄
ConfigsInitializingUserList = 初始化Mapper列表...
ConfigsUserListExplain = Mapper列表用來儲存你最喜愛的做圖者的名單。本程式會遍歷整個列表，把每個Mapper的所有作品全部下載下來。本列表一行寫一個Mapper的用戶名，不分大小寫。
ConfigsUserListPrompt = 你想現在編輯Mapper列表文件嗎?


# Editor

EditorNotFoundPrompt = 沒有找到默認的文本編輯器，你想現在設置一個嗎?
EditorMenuNotepad = 默認使用系統提供的記事本工具
EditorMenuNotepad2 = 下載Notpad2，一款輕量級的代碼編輯工具
EditorMenuSpecify = 手動指定一個文本編輯器
EditorMenuTitle = 設置你的默認文本編輯器
EditorMenuSpecifyPrompt = 請輸入編輯器的命令或絕對地址
EditorInstalling = 正在安裝文本編輯器...


# Node

NodeNotFoundPrompt = 沒有找到Node.js，你想現在進行下載並安裝嗎?
NodeRebootExplain = 你需要重啟計算機完成Node.js的安裝過程。
NodeRebootPrompt = 你想現在重啟計算機嗎?
NodeVersionUnsupportedLeft = 你的Node.js版本號
NodeVersionUnsupportedRight = 不兼容本程式。
NodeMinimumVersionLeft = Node.js版本號要求為
NodeMinimumVersionRight = 或更高。
NodeVersionPrompt = 你想現在下載並更新至最新的Node.js版本嗎?
NodeNpmNotFound = 看起來你的Node.js沒有正確安裝（找不到npm）
NodeNpmPrompt = 你想現在下載並重新安裝Node.js嗎?
NodeInstallingNpmModules = 正在安裝npm模塊...
NodeInstallingNode = 正在安裝Node.js...


# Utils

UtilsCleaningUp = 正在清除用戶數據...
UtilsMenuQuit = 退出或返回
UtilsMenuPrompt = 輸入你的選擇然後按回車
UtilsMenuDefautIs = 默認為
UtilsMenuInvalidInput = 無效的輸入，請再次輸入
