﻿Function Check-Configs {
    Function Init-Configs {
        Clear-Host
        Write-Host $I18N.ConfigsInitializingSettings
        $Filepath = (Join-Path $LIB_DIR "$( $I18N.Locale )\$CONFIG_SAMPLE_FILE")
        if (!(Test-Path $Filepath)) {
            $Filepath = (Join-Path $LIB_DIR "$DEFAULT_LOCALE\$CONFIG_SAMPLE_FILE")
        }
        Copy-Text-File $Filepath $CONFIG_FILE
        Configure-Api-Key
        if (Configure-Osu-Service) {
            Configure-Osu-Login
        }
        Configure-Osu-Dir
        Configure-Download-Dir
    }

    Function Configure-Api-Key {
        Write-Host $I18N.ConfigsApiRequired
        Write-Host $I18N.ConfigsApiApply
        if (Yes-Or-No $I18N.ConfigsApiWebPrompt -Y) {
            Start-Process "https://old.ppy.sh/p/api"
        }
        $Key = Read-Host $I18N.ConfigsApiPrompt
        Replace-Text-File $CONFIG_FILE "THE_API_KEY" $Key
    }

    Function Configure-Osu-Service {
        Clear-Host
        Write-Host $I18N.ConfigsOsuRequired
        Write-Host $I18N.ConfigsOsuExplain
        if (Yes-Or-No $I18N.ConfigsOsuPrompt) {
            Replace-Text-File $CONFIG_FILE "# 'osu'" "'osu'"
            $true
        }
    }

    Function Configure-Osu-Login {
        Write-Host $I18N.ConfigsOsuLoginTitle
        $Username = Read-Host $I18N.ConfigsOsuLoginUsername
        $Password = [Runtime.InteropServices.Marshal]::PtrToStringAuto(
                    [Runtime.InteropServices.Marshal]::SecureStringToBSTR((
                    Read-Host $I18N.ConfigsOsuLoginPassword -AsSecureString)))
        Replace-Text-File $CONFIG_FILE "THE_USERNAME" $Username
        Replace-Text-File $CONFIG_FILE "THE_PASSWORD" $Password
    }

    Function Configure-Osu-Dir {
        Clear-Host
        Write-Host $I18N.ConfigsOsuDirExplain
        Write-Host $I18N.ConfigsOsuDirExample
        $Path = (Read-Host $I18N.ConfigsOsuDirPrompt) -replace "\\", "\\"
        Replace-Text-File $CONFIG_FILE "THE_OSU_DIR" $Path
        Replace-Text-File $CONFIG_FILE "THE_SONGS_DIR" "$Path\\Songs"
    }

    Function Configure-Download-Dir {
        Clear-Host
        Write-Host $I18N.ConfigsDownloadDirExplain
        Write-Host $I18N.ConfigsDownloadDirExample
        $Path = (Read-Host $I18N.ConfigsDownloadDirPrompt) -replace "\\", "\\"
        Replace-Text-File $CONFIG_FILE "THE_DOWNLOAD_DIR" $Path
    }

    if (!(Test-Path $CONFIG_FILE)) {
        Init-Configs
    }
}

Function Check-User-List {
    Function Init-User-List {
        Clear-Host
        Write-Host $I18N.ConfigsInitializingUserList
        $Filepath = (Join-Path $LIB_DIR "$( $I18N.Locale )\$USER_LIST_SAMPLE_FILE")
        if (!(Test-Path $Filepath)) {
            $Filepath = (Join-Path $LIB_DIR "$DEFAULT_LOCALE\$USER_LIST_SAMPLE_FILE")
        }
        Copy-Text-File $Filepath $USER_LIST_FILE
        Write-Host $I18N.ConfigsUserListExplain
        if (Yes-Or-No $I18N.ConfigsUserListPrompt -Y) {
            Edit-Text-File $USER_LIST_FILE
        }
    }

    if (!(Test-Path $USER_LIST_FILE)) {
        Init-User-List
    }
    
}
