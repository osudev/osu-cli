Locale = en-US


# Main Menu

MainMenuTitle = Welcome to osu!cli Beatmap Downloader!
MainMenuStartDownload = Start download
MainMenuEditConfig = Edit config file
MainMenuEditUserList = Edit user list file
MainMenuOpenDownloadFolder = Open the download folder
MainMenuSetupEditor = Setup default text editor


# Constants

ConstConsoleTitle = osu!cli Beatmap Downloader


# Configs

ConfigsInitializingSettings = Initializing settings...
ConfigsApiRequired = The program requires an osu! API key.
ConfigsApiApply = You can apply one through https://old.ppy.sh/p/api \nYou can just make up the application name and URL.\nDon't worry no harm will come to your account from this.
ConfigsApiWebPrompt = Do you want to open the website now?
ConfigsApiPrompt = Right click and paste your API key here, then press ENTER
ConfigsOsuRequired = The osu! official download service needs your osu! login username and password.
ConfigsOsuExplain = If the service is enabled, it will either read the authentication data from the config file if it's set in this process, or it will ask you to type in every time if you leave them blank.
ConfigsOsuPrompt = Do you want to enable official osu! download service?
ConfigsOsuLoginTitle = Please enter your osu! login data.
ConfigsOsuLoginUsername = Username
ConfigsOsuLoginPassword = Password (Hidden)
ConfigsOsuDirExplain = In order to filter out the installed beatmaps, the program needs your osu! installation directory.
ConfigsOsuDirExample = For example: D:\\osu!
ConfigsOsuDirPrompt = The osu! installation directory
ConfigsDownloadDirExplain = Please specify a directory to save the downloaded beatmaps. You can just use the Songs folder in the osu! installation directory in order to make the downloaded beatmaps auto installed, but it may be buggy, so it's better to use a separate folder.
ConfigsDownloadDirExample = For example: E:\\Downloads\\Beatmaps
ConfigsDownloadDirPrompt = The download folder
ConfigsInitializingUserList = Initializing user list...
ConfigsUserListExplain = The user list is the place you put all your favorite mappers. The program will walk through the list and grab their latest beatmap releases.
ConfigsUserListPrompt = Do you want to edit the user list now?


# Editor

EditorNotFoundPrompt = A default text editor was not found, do you want to setup one?
EditorMenuNotepad = Use the system provided Notepad as default
EditorMenuNotepad2 = Download Notpad2 - a lightweight code editor
EditorMenuSpecify = Specify a text editor on your system
EditorMenuTitle = Setup your default text editor.
EditorMenuSpecifyPrompt = The editor's command or full path
EditorInstalling = Installing Text Editor...


# Node

NodeNotFoundPrompt = Node.js was not found. Do you want to download and install it now?
NodeRebootExplain = You need to reboot your machine to complete the Node.js installation process.
NodeRebootPrompt = Do you want to reboot your machine now?
NodeVersionUnsupportedLeft = Your Node.js version 
NodeVersionUnsupportedRight = is not supported.
NodeMinimumVersionLeft = Minimum version of 
NodeMinimumVersionRight = is required.
NodeVersionPrompt = Do you want to download and update to the latest Node.js version?
NodeNpmNotFound = It seems like your Node.js installation is broken. (npm not found)
NodeNpmPrompt = Do you want to download and re-install Node.js?
NodeInstallingNpmModules = Installing npm modules...
NodeInstallingNode = Installing Node.js...


# Utils

UtilsCleaningUp = Cleaning up...
UtilsMenuQuit = Quit or go back
UtilsMenuPrompt = Type your choice and press Enter
UtilsMenuDefautIs = Default is
UtilsMenuInvalidInput = Invalid input. Please tyr again.
