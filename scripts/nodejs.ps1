﻿Function Get-Node {
    if (If-Command-Exist $NODE) {
        (Get-Command $NODE).Definition
    } else {
        if (If-Command-Exist $VENDOR_NODE) {
            $VENDOR_NODE
        }
    }
}

Function Get-Npm {
    if (If-Command-Exist $NPM) {
        $NPM
    } else {
        if (If-Command-Exist $VENDOR_NPM) {
            $VENDOR_NPM
        }
    }
}

Function Check-Node {
    Function Node-Exists {
        if (Get-Node) {
            $true
        } else {
            if (Yes-Or-No -Prompt $I18N.NodeNotFoundPrompt -Y) {
                Install-Node
                Check-Node
            }
        }
    }

    Function Node-Valid {
        $NodeVersion = [Version](gci (Get-Node)).VersionInfo.ProductVersion
        if ($NodeVersion -ge $NODE_VERSION) {
            $true
        } else {
            Write-Host "$( $I18N.NodeVersionUnsupportedLeft ) $NodeVersion $( $I18N.NodeVersionUnsupportedRight )"
            Write-Host "$( $I18N.NodeMinimumVersionLeft ) $NODE_VERSION $( $I18N.NodeMinimumVersionRight )"
            if (Yes-Or-No -Prompt $I18N.NodeVersionPrompt -Y) {
                Install-Node
                Check-Node
            }
        }
    }

    Function Npm-Exists {
        if (Get-Npm) {
            $true
        } else {
            Write-Host $I18N.NodeNpmNotFound
            if (Yes-Or-No -Prompt $I18N.NodeNpmPrompt -Y) {
                Install-Node
                Check-Node
            }
        }
    }

    (Node-Exists) -and (Node-Valid) -and (Npm-Exists)
}

Function Check-Node-Modules {
    if (!(Test-Path $NODE_MODULES_DIR)) {
        Clear-Host
        Write-Host $I18N.NodeInstallingNpmModules
        Start-Process -FilePath (Get-Npm) -ArgumentList "install" -NoNewWindow -Wait
    }
}

Function Install-Node {
    Clear-Host
    Write-Host $I18N.NodeInstallingNode
    $Url = $NODE_URL[$ARCH]
    Download $Url $VENDOR_NODE
    $NpmZip = My-Join-Path $VENDOR_DIR ([System.IO.Path]::GetFileName($NPM_URL))
    Download $NPM_URL $NpmZip
    Unzip $NpmZip $VENDOR_NPM_DIR
    Copy-Item (Join-Path $VENDOR_NPM_DIR "npm\bin\npm.cmd") $VENDOR_NPM
    Remove-Item $NpmZip
    Refresh-Env
}
