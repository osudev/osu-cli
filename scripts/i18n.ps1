﻿Function Load-Locale ($Locale) {
  $LocalePath = Join-Path $SCRIPTS_DIR "$locale\i18n.psd1"
  if (Test-Path $LocalePath) {
    ConvertFrom-StringData (Read-Text-File $LocalePath)
  } else {
    @{}
  }
}

$I18N = Merge-Hashtables (Load-Locale $DEFAULT_LOCALE) (Load-Locale ((Get-Culture).Name))
