﻿if (!$PSScriptRoot) {
    $PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
}

. $PSScriptRoot\utils
. $PSScriptRoot\consts
. $PSScriptRoot\i18n
. $PSScriptRoot\update
. $PSScriptRoot\nodejs
. $PSScriptRoot\config
. $PSScriptRoot\editor

Function Main-Menu {
    $Continue = $true
    while($Continue) {
        $MainMenuOptions = @(
            <# 1 #> $I18N.MainMenuStartDownload,
            <# 2 #> $I18N.MainMenuEditConfig,
            <# 3 #> $I18N.MainMenuEditUserList,
            <# 4 #> $I18N.MainMenuOpenDownloadFolder,
            <# 5 #> $I18N.MainMenuSetupEditor
        )
        $Answer = Create-Menu -Title $I18N.MainMenuTitle -Options $MainMenuOptions -Default 1
        Switch ($Answer) {
            1 { # Start download
                Start-Process -FilePath (Get-Node) -ArgumentList $ROOT_DIR -NoNewWindow -Wait
                pause
            }
            2 { # Edit config file
                Edit-Text-File $CONFIG_FILE
            }
            3 { # Edit user list file
                Edit-Text-File $USER_LIST_FILE
            }
            4 { # Open the download folder
                $pinfo = New-Object System.Diagnostics.ProcessStartInfo
                $pinfo.FileName = (Get-Node)
                $pinfo.Arguments =
                    (My-Join-Path $NODE_MODULES_DIR "LiveScript\bin\lsc"),
                    "$LIB_DIR\get_config.ls",
                    "download_dir"
                $pinfo.UseShellExecute = $false
                $pinfo.CreateNoWindow = $true
                $pinfo.RedirectStandardOutput = $true
                $pinfo.RedirectStandardError = $true
                $process = New-Object System.Diagnostics.Process
                $process.StartInfo = $pinfo
                $process.Start() | Out-Null
                $process.WaitForExit()
                $stdout = $process.StandardOutput.ReadToEnd()
                $dirpath = $stdout -replace "`n|`r"
                ii $dirpath
            }
            5 {
                $Editor = Setup-Text-Editor-Menu
            }
            Default { # Quit
                $Continue = $false
            }
        }
    }
}

$host.ui.RawUI.WindowTitle = $I18N.ConstConsoleTitle
Set-Location $ROOT_DIR

# Dev-Clean

Check-Update

if (!(Check-Node)) {
    exit
}

Check-Configs
Check-User-List
Check-Node-Modules

Main-Menu
