Function Check-Update {
         Function Install-Update {
                  Install-Vendor-Zip $UPDATE_URL $ROOT_DIR
                  Start-Process (Join-Path $ROOT_DIR "start.cmd")
                  exit
         }

         $LocalPackageJson = Load-JSON-File $LOACL_PACKAGE_JSON_FILE
         $RemotePakageJson = ConvertFrom-JSON (Invoke-WebRequest $REMOTE_PACKAGE_JSON_URL)
         $LocalVersion = [version]$LocalPackageJson.version
         $RemoteVersion = [version]$RemotePackageJson.version
         if ($LocalVersion -lt $RemoteVersion) {
            if (Yes-Or-No "An update was found. Do you want to update now?" -Y) {
               Install-Update
            }
         }
}