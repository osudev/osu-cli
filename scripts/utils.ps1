﻿Function Merge-Hashtables ($htold, $htnew) {
    $keys = $htold.getenumerator() | foreach-object {$_.key}
    $keys | foreach-object {
        $key = $_
        if ($htnew.containskey($key))
        {
            $htold.remove($key)
        }
    }
    $htold + $htnew
}

Function Read-Text-File ($Filepath) {
    [Io.File]::ReadAllText($Filepath, [Text.Encoding]::UTF8)
}

Function Write-Text-File ($Filepath, $Content) {
    [System.IO.File]::WriteAllLines($Filepath, $Content, [Text.Encoding]::UTF8)
}

Function Load-JSON-File ($Filepath) {
    ConvertFrom-Json (Read-Text-File $Filepath)
}

Function Save-JSON-File ($Object, $Filepath) {
    Write-Text-File $Filepath (ConvertTo-Json $Object)
}

Function Set-Property ($Object, $Key, $Value) {
    $Object | Add-Member -Name $Key -Value $Value -MemberType NoteProperty -Force
}

Function Remove-Property ($Object, $Key) {
    $Object | Select-Object -Property * -ExcludeProperty $Key
}

Function Save-Globals {
    Save-JSON-File $GLOBALS $GLOBALS_FILE
}

Function Set-Global ($Key, $Value) {
    Set-Property $GLOBALS $Key $Value
    Save-Globals
}

Function Remove-Global ($Key) {
    $GLOBALS = Remove-Property $GLOBALS $Key
    Save-Globals
}

Function My-Join-Path ($Base, $Relative) {
    return [System.IO.Path]::GetFullPath((Join-Path $Base $Relative))
}

Function Dev-Clean {
    Clear-Host
    Write-Host $I18N.UtilsCleaningUp
    Remove-Directory $DATA_DIR, $VENDOR_DIR, $NODE_MODULES_DIR
}

Function Copy-Text-File ($Source, $Destination) {
    Make-Directory (Split-Path $Destination)
    Write-Text-File $Destination (Read-Text-File $Source)
}

Function Unzip ($ZipFile, $Destination) {
    $Shell = New-Object -Com Shell.Application
    $Shell.Namespace($Destination).CopyHere($Shell.NameSpace($ZipFile).Items(), 16)
}

Function Install-Vendor-Zip ($Url, $MoveTo, [switch]$KeepZip) {
    $ZipFile = Join-Path $VENDOR_DIR ([System.IO.Path]::GetFileName($Url))
    Download $Url $ZipFile
    Unzip $ZipFile $VENDOR_DIR
    $DirName = (New-Object -Com Shell.Application).NameSpace($ZipFile).Items().Item(0).Name
    $DirPath = Join-Path $VENDOR_DIR $DirName
    if ($MoveTo) {
        Move-Item $DirPath $MoveTo
    } else {
        $DirPath
    }
    if (!$KeepZip) {
        Remove-Item $ZipFile -Force
    } else {
        $ZipFile
    }
}

function Invoke-Admin() {
    param ( [string]$FilePath = $(throw "Please specify a program" ),
            [string]$Arguments = "",
            [switch]$Wait )

    $psi = New-Object System.Diagnostics.ProcessStartInfo
    $psi.FileName = $FilePath 
    $psi.Arguments = $Arguments
    $psi.UseShellExecute = $false
    $psi.CreateNoWindow = $true
    $psi.RedirectStandardOutput = $true
    $psi.RedirectStandardError = $true
    $proc = New-Object System.Diagnostics.Process
    $proc.StartInfo = $psi
    $proc.Start() | Out-Null
    if ( $Wait ) {
        $proc.WaitForExit();
    }
}

Function Download ($Url, $Destination) {
    Write-Host "Downloading $URL to $Destination"
    Make-Directory (Split-Path $Destination)
    Start-BitsTransfer $URL $Destination
}

Function Refresh-Env {
    $env:Path = (Get-Env "Path" -Profile "Machine") + ';' + (Get-Env "Path")
}

Function Which ($Command) {
    Get-Command $Command | Select-Object -ExpandProperty Definition
}

Function Select-Folder($Prompt = "Please select a folder", $Path = 0) {
    $Shell = New-Object -ComObject Shell.Application
    $Folder = $Shell.BrowseForFolder(0, $Prompt, 0, $Path)
    if ($Folder -ne $null) {
        $Folder.self.Path
    }
}

Function Yes-Or-No ($Prompt, [switch]$Y, [switch]$N) {
    while($true) {
        $Prefer = $Y -xor $N
        $Answer = Read-Host "$Prompt [$(@('y';'Y')[$Prefer -and $Y.IsPresent])/$(@('n';'N')[$Prefer -and $N.IsPresent])]"
        Switch ($Answer) {
            {($_ -eq "y") -or ($_ -eq "yes")} {return $true }
            {($_ -eq "n") -or ($_ -eq "no" )} {return $false}
            Default {
                if (!$Answer -and $Prefer) {
                    if ($Y) {return $true }
                    if ($N) {return $false}
                }
            }
        }
    }
}

Function Set-Env ($Key, $Value, $Profile = "User") {
    [Environment]::SetEnvironmentVariable($Key, $Value, $Profile)
}

Function Get-Env ($Key, $Profile = "User") {
    [Environment]::GetEnvironmentVariable($Key, $Profile)
}

Function Create-Menu ([string]$Title, [array]$Options, [string]$Default = "") {
    Clear-Host
    Write-Host $Title
    Write-Host ""
    $Options | % {$i=1} {
        Write-Host "  [$i] $_"
        $i++
    }
    Write-Host "  [Q] $( $I18N.UtilsMenuQuit )"
    while($true) {
        Write-Host -NoNewline "`n$( $I18N.UtilsMenuPrompt )"
        if ($Default) {
            Write-Host -NoNewline " ($( $I18N.UtilsMenuDefautIs ) $Default)"
        }
        Write-Host -NoNewline ": "
        $Answer = Read-Host
        If (!$Answer) {$Answer = $Default}
        if ($Answer -eq "q") {
            $false
            break
        }
        Try {
            if($Answer) {
                $i = $Answer - 1
                if ($Options[$i++]) {
                    $i
                    break
                }
            }
        }
        Catch [System.Exception] {}
        Write-Host $I18N.UtilsMenuInvalidInput
    }
}

Function Replace-Text-File ($Filepath, $Find, $Subs) {
    Write-Text-File $Filepath ((Read-Text-File $Filepath) | 
        Foreach-Object {$_ -replace $Find, $Subs})
}

Function Make-Directory ($Path) {
    New-Item -Force -Path $Path -Type Directory -ErrorAction SilentlyContinue | Out-Null
}

Function Remove-Directory ($Path) {
    Remove-Item -Path $Path -Recurse -Force -ErrorAction SilentlyContinue | Out-Null
}

Function If-Command-Exist ($Command) {
    [bool](& {if ($Command) {
        Get-Command $Command -ErrorAction SilentlyContinue
    }})
}
