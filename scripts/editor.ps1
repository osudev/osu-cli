﻿Function Edit-Text-File ($Filepath) {
    if ($Editor = Get-Text-Editor) {
        Write-Host "Editing $Filepath..."
        Start-Process -FilePath $Editor -ArgumentList $Filepath -Wait
    }
}

Function Get-Text-Editor {
    Function Init-Text-Editor {
        Clear-Host
        if (Yes-Or-No $I18N.EditorNotFoundPrompt -Y) {
            Setup-Text-Editor-Menu
            Get-Text-Editor
        }
    }

    # Check the user prefered editor
    if (If-Command-Exist ($PreferedEditor = Get-Env "EDITOR")) {
        $PreferedEditor
    } else {
        # Check the vendor editor
        if (Test-Path $VENDOR_TEXT_EDITOR) {
            $VENDOR_TEXT_EDITOR
        } else {
            Init-Text-Editor
        }
    }
}

Function Setup-Text-Editor-Menu {
    $MainMenuOptions = @(
        <# 1 #> $I18N.EditorMenuNotepad,
        <# 2 #> $I18N.EditorMenuNotepad2,
        <# 3 #> $I18N.EditorMenuSpecify
    )
    $Answer = Create-Menu -Title $I18N.EditorMenuTitle -Options $MainMenuOptions -Default 1
    Switch ($Answer) {
        1 { # Use Notepad as default
            Set-Env "EDITOR" "notepad"
        }
        2 { # Install and use Notpad2
            Install-Vendor-Text-Editor
            Set-Env "EDITOR" $VENDOR_TEXT_EDITOR
        }
        3 { # Enter a text editor location
            Set-Env "EDITOR" (Read-Host $I18N.EditorMenuSpecifyPrompt)
        }
    }
}

Function Install-Vendor-Text-Editor {
    Clear-Host
    Write-Host $I18N.EditorInstalling
    $ZipFile = My-Join-Path $VENDOR_DIR ([System.IO.Path]::GetFileName($VENDOR_TEXT_EDITOR_URL))
    Download $VENDOR_TEXT_EDITOR_URL $ZipFile
    Unzip $ZipFile $VENDOR_TEXT_EDITOR_DIR
    Remove-Item $ZipFile
}
