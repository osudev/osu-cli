Locale = zh-CN


# Main Menu

MainMenuTitle = 欢迎使用 osu!cli 下图工具！
MainMenuStartDownload = 开始下载
MainMenuEditConfig = 编辑配置文件
MainMenuEditUserList = 编辑Mapper列表文件
MainMenuOpenDownloadFolder = 打开下载目录
MainMenuSetupEditor = 设置默认文本编辑器


# Constants

ConstConsoleTitle = osu!cli 下图工具


# Configs

ConfigsInitializingSettings = 正在进行初始化设置...
ConfigsApiRequired = 本程序需要设置osu!开放接口认证码。
ConfigsApiApply = 你可以访问 https://old.ppy.sh/p/api 申请一枚。\nApplication Name和URL可以随便填写。\n申请认证码不会对你的账号造成任何影响，请放心操作。
ConfigsApiWebPrompt = 你想现在打开认证码申请页面吗?
ConfigsApiPrompt = 复制网页上的开放接口认证码（API Key），右键本窗口进行粘贴，然后按回车
ConfigsOsuRequired = osu!官方的下载服务需要进行用户验证，你需要提供你的osu!用户名和密码。
ConfigsOsuExplain = 如果不启用该下载服务，则不需要提供登录信息。如果启用，本程序则会从配置文件中读取你的登录信息。你可以在本初始化引导过程中填写这些项目，但是你的密码将以明文的方式储存。如果你觉得不妥，可以留空这些项目，让程序在每次执行的时候询问你进行输入。
ConfigsOsuPrompt = 你想启用osu!官方的下载服务吗?
ConfigsOsuLoginTitle = 请输入你的osu!登录信息
ConfigsOsuLoginUsername = 用户名
ConfigsOsuLoginPassword = 密码 （隐藏输入）
ConfigsOsuDirExplain = 如果你提供osu!的安装目录，下载前程序会自动跳过已经存在的图。
ConfigsOsuDirExample = 例如： D:\\osu!
ConfigsOsuDirPrompt = osu!安装目录
ConfigsDownloadDirExplain = 请指定一个目录用来保存下好的图。你可以将这个目录设置成你osu!的Songs目录，以便让下好的图自动添加到曲库，但是这样做有可能会与osu!主程序产生冲突，最好的方式还是使用一个独立的目录进行储存，等下载结束后再导入游戏中。
ConfigsDownloadDirExample = 例如： E:\\Downloads\\Beatmaps
ConfigsDownloadDirPrompt = 下载目录
ConfigsInitializingUserList = 初始化Mapper列表...
ConfigsUserListExplain = Mapper列表用来储存你最喜爱的做图者的名单。本程序会遍历整个列表，把每个Mapper的所有作品全部下载下来。本列表一行写一个Mapper的用户名，不分大小写。
ConfigsUserListPrompt = 你想现在编辑Mapper列表文件吗?


# Editor

EditorNotFoundPrompt = 没有找到默认的文本编辑器，你想现在设置一个吗?
EditorMenuNotepad = 默认使用系统提供的记事本工具
EditorMenuNotepad2 = 下载Notpad2，一款轻量级的代码编辑工具
EditorMenuSpecify = 手动指定一个文本编辑器
EditorMenuTitle = 设置你的默认文本编辑器
EditorMenuSpecifyPrompt = 请输入编辑器的命令或绝对地址
EditorInstalling = 正在安装文本编辑器...


# Node

NodeNotFoundPrompt = 没有找到Node.js，你想现在进行下载并安装吗?
NodeRebootExplain = 你需要重启计算机完成Node.js的安装过程。
NodeRebootPrompt = 你想现在重启计算机吗?
NodeVersionUnsupportedLeft = 你的Node.js版本号
NodeVersionUnsupportedRight = 不兼容本程序。
NodeMinimumVersionLeft = Node.js版本号要求为
NodeMinimumVersionRight = 或更高。
NodeVersionPrompt = 你想现在下载并更新至最新的Node.js版本吗?
NodeNpmNotFound = 看起来你的Node.js没有正确安装（找不到npm）
NodeNpmPrompt = 你想现在下载并重新安装Node.js吗?
NodeInstallingNpmModules = 正在安装npm模块...
NodeInstallingNode = 正在安装Node.js...


# Utils

UtilsCleaningUp = 正在清除用户数据...
UtilsMenuQuit = 退出或返回
UtilsMenuPrompt = 输入你的选择然后按回车
UtilsMenuDefautIs = 默认为
UtilsMenuInvalidInput = 无效的输入，请再次输入
