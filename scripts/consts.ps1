﻿$ROOT_DIR = My-Join-Path $PSScriptRoot ".."
$LIB_DIR = My-Join-Path $ROOT_DIR "lib"
$DATA_DIR = My-Join-Path $ROOT_DIR "data"
$VENDOR_DIR = My-Join-Path $ROOT_DIR "vendor"
$SCRIPTS_DIR = My-Join-Path $ROOT_DIR "scripts"
$NODE_MODULES_DIR = My-Join-Path $ROOT_DIR "node_modules"

$CONFIG_FILE = My-Join-Path $DATA_DIR "config.ls"
$USER_LIST_FILE = My-Join-Path $DATA_DIR "user_list.txt"
$CONFIG_SAMPLE_FILE = "config.sample.ls"
$USER_LIST_SAMPLE_FILE = "user_list.sample.txt"
$LOACL_PACKAGE_JSON_FILE = My-Join-Path $ROOT_DIR "package.json"

$NODE = 'node'
$NPM = 'npm'
$VENDOR_NODE_DIR = My-Join-Path $VENDOR_DIR "node"
$VENDOR_NODE = My-Join-Path $VENDOR_NODE_DIR "node.exe"
$VENDOR_NPM_DIR = My-Join-Path $VENDOR_NODE_DIR "node_modules"
$VENDOR_NPM = My-Join-Path $VENDOR_NODE_DIR "npm.cmd"
$NODE_VERSION = [Version]'0.10.0'
$NPM_URL = "https://bitbucket.org/osudev/osu-cli/raw/static/npm.zip"
$NODE_URL = @{
    "x86" = "https://bitbucket.org/osudev/osu-cli/raw/static/node-x86.exe"
    "amd64" = "https://bitbucket.org/osudev/osu-cli/raw/static/node-x64.exe"
}

$ARCH = $ENV:PROCESSOR_ARCHITECTURE

$VENDOR_TEXT_EDITOR = My-Join-Path $VENDOR_DIR "notepad2/notepad2.exe"
$VENDOR_TEXT_EDITOR_DIR = My-Join-Path $VENDOR_DIR "notepad2"
$VENDOR_TEXT_EDITOR_URL = "https://bitbucket.org/osudev/osu-cli/raw/static/notepad2.zip"

$REMOTE_PACKAGE_JSON_URL = "https://bitbucket.org/osudev/osu-cli/raw/master/package.json"
$UPDATE_URL = "https://bitbucket.org/osudev/osu-cli/get/master.zip"
$NODE_MODULES_URL = "https://bitbucket.org/osudev/osu-cli/raw/static/node_modules.zip"

$DEFAULT_LOCALE = "en-US"
