@echo off

title osu!cli Beatmap Downloader

:: Define constants
set POWERSHELL="powershell"
set SCRIPTS_DIR="%~dp0\scripts"


:: Start processing
call:check_powershell
call %POWERSHELL% -Command "Set-ExecutionPolicy -Force Unrestricted -Scope CurrentUser -Confirm:$false -ErrorAction SilentlyContinue | Out-Null"
call %POWERSHELL% -ExecutionPolicy Bypass -Command ". ""%SCRIPTS_DIR%\index"""
exit /b 0


:: Workflows

:check_powershell
  ver > nul
  WHERE %POWERSHELL% > NUL 2>&1
  IF %ERRORLEVEL% NEQ 0 (
    call:powershell_not_found
    exit
  )
goto:eof

:powershell_not_found
  color 0C
  set /P ANSWER=PowerShell was not found. Do you want to open the download website? (Y/n) 
  if /i {%ANSWER%}=={n} exit /b 1
  start "" "http://go.microsoft.com/fwlink/?LinkId=293881"
goto:eof
