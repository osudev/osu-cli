# 本文件由程序自动生成，你可以通过修改本文件更改程序的配置

configs = 
  # 设置osu!开放接口认证码（API Key）
  # 你可以通过下面的链接进行申请
  # https://old.ppy.sh/p/api
  # 申请需要填写的程序名称（Application Name）和ULR可以随便填写
  # 申请认证码不会对你的账号造成任何影响，请放心使用
  api_key: 'THE_API_KEY'

  # 设置osu!的安装目录
  # 请务必将所有反斜杠写成两个！！！
  # 例如: 'D:\\osu!'
  osu_dir: 'THE_OSU_DIR'

  # 设置osu!安装目录下的Songs目录
  # 请务必将所有反斜杠写成两个！！！
  # 例如: 'D:\\osu!\\Songs'
  songs_dir: 'THE_SONGS_DIR'

  # 设置下载保存Beatmap的目录
  # 请务必将所有反斜杠写成两个！！！
  # 例如: 'E:\\Downloads\\Beatmaps'
  download_dir: 'THE_DOWNLOAD_DIR'

  # 设置Mapper订阅列表文件
  user_list_file: './data/user_list.txt'

  # 设置是否跳过已下载的文件
  skip_downloaded: true

  # 设置是否跳过已知的无效Beatmaps
  # 当程序无法找到有效的下载链接，该图的Set ID就会被记录下来
  # 当下一次遇到时就可以选择是否跳过该图
  skip_bad_set_id: true

  # 如果你启用osu!官网的下载服务，你需要在这里设置你的osu!账号登录信息
  # 如果在本配置中留空某一项，则程序会在运行时提示你进行手动输入
  # 如果你在下面的配置中关闭osu!下载服务，则程序不会理会本设置项
  osu_login:
    username: 'THE_USERNAME'
    password: 'THE_PASSWORD'

  # 下载程序会从上倒下地遍历下面的几个下载提供商，寻找可用的下载地址
  # 更改列表的先后顺序可以帮助你设置最优先选用的提供商
  service_priority:
    # 删除下一行开头的井号和跟着的一个空格可以启用osu!官网的下载服务
    # 'osu'
    'uu_gl'
    'loli_al'
    'bloodcat'

  # 你可以在下面自定义Beatmap下载任务的过滤函数（仅限高级用户）
  # filter: (beatmap_set_info, api) ->
  #   beatmap_set_info.beatmapset_id is not '123456'

module.exports = configs