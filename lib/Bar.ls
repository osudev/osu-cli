require! colors

class Bar
  now = -> (new Date()).getTime()

  clear = ->
    process.stdout.write '\u001b[2J\u001b[0;0H'

  write = (line, newLine) ->
    process.stdout.write line
    process.stdout.write '\n' if newLine

  wrap = (text, length) ->
    return '' if length < 4
    return text unless length < text.length
    return (text.substr 0, length - 3) + '...'

  throttle = 100

  (title, total_length) ->
    @title = title or ''

    @first_tick = @last_tick = now()

    @buffer_size = 0
    @received_length = 0
    @total_length = total_length or 0

    @bar_warp = '[]'
    @bar_elapsed_char = '='
    @bar_remain_char = '-'

  tick: (chunk_length) ->
    @buffer_size += chunk_length
    @received_length += chunk_length
    time_now = now()
    time_elapsed = time_now - @last_tick
    return if (time_elapsed < throttle)
    @update {time_now}

  finish: ->
    time_now = now()
    @buffer_size = @total_length - @received_length
    @received_length = @total_length
    speed_per_second = humanFileSize(@total_length / ((time_now - @first_tick) / 1000)) + '/s'
    color = (text) -> colors.bgWhite colors.blue text
    @update {time_now, speed_per_second, color}

  update: (options) ->
    time_now = options.time_now or now()
    time_elapsed = options.time_elapsed or time_now - @last_tick
    color = (text) -> colors.bgWhite colors.black text
    color = options.color or color
    file_size = humanFileSize @total_length
    speed_per_second = options.speed_per_second or
      humanFileSize(@buffer_size / (time_elapsed / 1000)) + '/s'
    percentage = Math.round(@received_length * 100 / @total_length) + '%'
    info = " #{percentage} #{speed_per_second} #{file_size}"
    bar_total_length = process.stdout.columns - info.length
    if bar_total_length > 2
      bar = color @bar_warp[0]
      bar_inner_length = bar_total_length - 2
      wrapped_title = wrap @title, bar_inner_length
      if @total_length
        elapsed_bar_length = Math.round @received_length / @total_length * bar_inner_length
      else
        elapsed_bar_length = 0
      remain_bar_length = bar_inner_length - elapsed_bar_length
      for i from 1 to elapsed_bar_length
        char = wrapped_title[i - 1] or @bar_elapsed_char[0]
        bar += color char
      for i from 1 to remain_bar_length
        char = wrapped_title[elapsed_bar_length + i - 1] or @bar_remain_char[0]
        bar += char
      bar += color @bar_warp[1]
    clear()
    write bar + info
    @buffer_size = 0
    @last_tick = time_now

humanFileSize = (bytes) ->
  thresh = 1024
  return bytes + 'B'  if bytes < thresh
  units = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  u = -1
  loop
    bytes /= thresh
    ++u
    break unless bytes >= thresh
  bytes.toFixed(1) + units[u]

module.exports = Bar
