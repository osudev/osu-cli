require! 'fs'
require! 'path'
require! 'async'
require! 'colors'
require! './Osu'
require! './Downloader'
_ = require 'underscore'

App =
  paths: {}

App.paths.root_dir = path.dirname require.main.filename
App.paths.data_dir = path.join App.paths.root_dir, 'data'
App.paths.config_file = path.join App.paths.data_dir, 'config'
App.paths.bad_set_id_list_file = path.join App.paths.data_dir, 'bad_set_id_list.json'

NOT_EXIST = 'Not exist'

module.exports = ->
  try
    config = require App.paths.config_file
  catch error
    if error.code is 'MODULE_NOT_FOUND'
      console.log 'Please copy or rename config.sample.ls to config.ls and edit the setting with text editor.'.red
    else
      console.log 'Please check again the config.ls is correctly written without syntax errors.'.red
      console.trace error
    process.exit 1

  try
    bad_set_id_list = require App.paths.bad_set_id_list_file
  catch error
    bad_set_id_list = []

  API = new Osu(config.api_key)
  downloader = new Downloader(config)

  add_bad_set_id = (set_id) ->
    if set_id not in bad_set_id_list
      bad_set_id_list.push parseInt set_id
      fs.writeFileSync App.paths.bad_set_id_list_file, JSON.stringify bad_set_id_list

  read_user_list = (file_path) ->
    fs.readFileSync file_path
      .toString()
      .replace /(\/\*([\s\S]*?)\*\/)|(\/\/(.*)$)/gm, ''
      .split '\n'
      .map (it) -> it.trim()
      .filter (it) -> !it.match /^\s*$/

  read_installed_beatmapset_id_list = (dir_path) ->
    file_list = fs.readdirSync dir_path
    id_list = []
    _.each file_list, (it) ->
      if matches = it.match /^\s*(\d+)((\s+.*)|(\s*))$/
        id_list.push parseInt matches[1]
    id_list

  user_list = read_user_list config.user_list_file

  installed_set_id_list = if config.songs_dir
    read_installed_beatmapset_id_list config.songs_dir
  else []

  if config.skip_downloaded
    installed_set_id_list = installed_set_id_list.concat do
      read_installed_beatmapset_id_list config.download_dir

  if config.skip_bad_set_id
    installed_set_id_list = installed_set_id_list.concat bad_set_id_list

  get_user_id = (user, callback) ->
    API.get_user u: user, (err, res) ->
      return callback err if err
      callback err, (res[0] && res[0].user_id) || null

  get_beatmapset_list = (user_id, callback) ->
    API.get_beatmaps u: user_id, callback

  async.map user_list, get_user_id, (err, user_id_list) ->
    _.filter user_id_list, (it) -> it?
    async.map user_id_list, get_beatmapset_list, (err, set_list) ->
      iteratee = (it) -> parseInt it.beatmapset_id
      set_list = _.filter do
        _.uniq do
          _.sortBy do
            _.flatten set_list, true
            iteratee
          iteratee
        (it) ->
          filter = config.filter or -> true
          filter it, API
      set_id_list = _.map set_list, iteratee
      set_tree = _.object set_id_list, set_list
      new_set_id_list = _.difference set_id_list, installed_set_id_list
      new_set_list = _.values _.pick set_tree, new_set_id_list

      size = _.size new_set_list
      count = 1
      async.reduce new_set_list, [], (memo, item, callback) ->
        prefix = "#{count++}/#{size}"
        console.log "#{prefix}: #{item.beatmapset_id} #{item.artist} - #{item.title}".yellow
        item.prefix = prefix
        downloader.download item, (err) ->
          if err is NOT_EXIST
            console.log 'Failed: Not available'.red
            memo.push item
            add_bad_set_id item.beatmapset_id
            return callback null, memo
          callback err, memo
      , (err, not_exist_set_list) ->
        console.log "#{--count}/#{size} All done.".green
        if not_exist_set_list.length
          console.log 'These beatmap sets are not available in all mirrors:'.red
          console.log not_exist_set_list.map iteratee
