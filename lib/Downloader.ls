require! 'fs'
require! 'path'
require! './Bar'
require! 'async'
require! 'colors'
require! 'request'
_ = require 'underscore'
readlineSync = require 'readline-sync'

NOT_EXIST = 'Not exist'
COMPLETED = 'Completed'

class Downloader

  jar = request.jar()
  request_with_jar = request.defaults jar: jar

  get_username = ->
    readlineSync.question 'Username: '.gray

  get_password = ->
    readlineSync.question 'Password: '.gray # , noEchoBack: true # Not working on Windows

  get_login = (service_name, config, hash_only = false) ->
    console.log "Your #{service_name} login info".gray
    unless config.username
      username = get_username()
      password = get_password()
    else
      username = config.username
      console.log "Username: #{username}".gray
      unless config.password
        password = get_password()
      else
        password = config.password
        console.log 'Password: *********'.gray
    {username, password}

  download = null

  (@config) ->
    download := (options, callback) ~>
      console.log "Trying: #{options.service}".magenta
      bar = msg = is_file = null
      downloading = false
      receiving_data = false
      total_length = 0
      fullname = "#{options.beatmapset_id} #{options.artist} - #{options.title}"
      filename = "#{fullname}.osz"
        .replace /[^A-Za-z 0-9 \.\,\!\@\#\$\%\^\&\(\)\-\_\=\+\;\[\]\`\~]/g, '_'
      save_path = path.resolve @config.download_dir, filename
      is_a_file = ->
        return is_file if is_file
        is_file := req.response.headers['content-type'].match /^application\//
      req = request_with_jar options.url
      req
        .on 'response', ->
          if is_a_file()
            msg := null
            unless downloading
              downloading := true
              console.log "Found available link from #{options.service}"
              req.response.pipe fs.createWriteStream save_path
          else
            msg := NOT_EXIST
            req.abort()
        .on 'data', (chunk) ->
          receiving_data := true
          return bar.tick chunk.length if bar
          content_length = req.response.headers['content-length']
          if content_length
            total_length := parseInt content_length
            bar := new Bar "#{options.prefix} (#{options.service}) #{fullname} by #{options.creator}", total_length
        .on 'end', ->
          unless is_a_file()
            msg := NOT_EXIST
          else
            msg := COMPLETED
            if bar
              bar.finish()
          callback null, msg
        .on 'error', (err) ->
          callback err, msg
    if 'osu' in @config.service_priority
      @osu_login = get_login 'osu!', @config.osu_login, true

  login_osu: (param, callback) ~>
    request_with_jar 'https://old.ppy.sh', ~>
      req = request_with_jar.post do
        url: 'https://old.ppy.sh/forum/ucp.php?mode=login'
        form:
          username: @osu_login.username
          password: @osu_login.password
          login: 'login'
        callback

  download: (options, callback) ~>
    services = _.reduce do
      @config.service_priority,
      (memo, service_name) ~>
        service_func = @["download_from_#{service_name}"]
        if service_func?
          memo.push service_func
        memo
      , []
    async.reduce services
    , null
    , (memo, item, callback) ->
      unless memo
        item options, callback
      else
        if memo is NOT_EXIST
          return item options, callback
        callback memo
    , (err, result) ->
      if err is COMPLETED
        return callback()
      if result is NOT_EXIST
        return callback result
      callback err

  download_from_osu: (options, callback) ~>
    @login_osu @osu_login, (err) ->
      return callback err if err
      options.url = "http://old.ppy.sh/d/#{options.beatmapset_id}"
      options.service = 'osu!'
      download options, callback

  download_from_bloodcat: (options, callback) ~>
    options.url = "http://bloodcat.com/osu/m/#{options.beatmapset_id}"
    options.service = 'bloodcat'
    download options, callback

  download_from_loli_al: (options, callback) ~>
    console.log 'Checking: loli.al'.magenta
    request "http://loli.al/osu.osp?action=check&type=set&id=#{options.beatmapset_id}", (err, res, data) ->
      return callback err if err
      loli_id = parseInt data.toString()
      if loli_id is 0
        return callback null, NOT_EXIST
      options.url = "http://loli.al/d/#{loli_id}/"
      options.service = 'loli.al'
      download options, callback

  download_from_uu_gl: (options, callback) ~>
    options.url = "http://osu.uu.gl/s/#{options.beatmapset_id}"
    options.service = 'uu.gl'
    download options, callback

humanFileSize = (bytes, si) ->
  thresh = (if si then 1000 else 1024)
  return bytes + ' B'  if bytes < thresh
  units =
    if si then
      ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    else
      ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
  u = -1
  loop
    bytes /= thresh
    ++u
    break unless bytes >= thresh
  bytes.toFixed(1) + ' ' + units[u]

module.exports = Downloader
