# 本文件由程序自動生成，你可以通過修改本文件更改程序的配置

configs = 
  # 設置osu!開放接口認證碼（API Key）
  # 你可以通過下面的鏈接進行申請
  # https://old.ppy.sh/p/api
  # 申請需要填寫的程序名稱（Application Name）和ULR可以隨便填寫
  # 申請認證碼不會對你的賬號造成任何影響，請放心使用
  api_key: 'THE_API_KEY'

  # 設置osu!的安裝目錄
  # 請務必將所有反斜杠寫成兩個！！！
  # 例如: 'D:\\osu!'
  osu_dir: 'THE_OSU_DIR'

  # 設置osu!安裝目錄下的Songs目錄
  # 請務必將所有反斜杠寫成兩個！！！
  # 例如: 'D:\\osu!\\Songs'
  songs_dir: 'THE_SONGS_DIR'

  # 設置下載保存Beatmap的目錄
  # 請務必將所有反斜杠寫成兩個！！！
  # 例如: 'E:\\Downloads\\Beatmaps'
  download_dir: 'THE_DOWNLOAD_DIR'

  # 設置Mapper訂閱列表文件
  user_list_file: './data/user_list.txt'

  # 設置是否跳過已下載的文件
  skip_downloaded: true

  # 設置是否跳過已知的無效Beatmaps
  # 當程序無法找到有效的下載鏈接，該圖的Set ID就會被記錄下來
  # 當下一次遇到時就可以選擇是否跳過該圖
  skip_bad_set_id: true

  # 如果你啟用osu!官網的下載服務，你需要在這裏設置你的osu!賬號登錄信息
  # 如果在本配置中留空某一項，則程序會在運行時提示你進行手動輸入
  # 如果你在下面的配置中關閉osu!下載服務，則程序不會理會本設置項
  osu_login:
    username: 'THE_USERNAME'
    password: 'THE_PASSWORD'

  # 下載程序會從上倒下地遍歷下面的幾個下載提供商，尋找可用的下載地址
  # 更改列表的先後順序可以幫助你設置最優先選用的提供商
  service_priority:
    # 刪除下一行開頭的井號和跟著的一個空格可以啟用osu!官網的下載服務
    # 'osu'
    'uu_gl'
    'loli_al'
    'bloodcat'

  # 你可以在下面自定義Beatmap下載任務的過濾函數（僅限高級用戶）
  # filter: (beatmap_set_info, api) ->
  #   beatmap_set_info.beatmapset_id is not '123456'

module.exports = configs