require! url
require! request

class Osu

  base_url = 'https://old.ppy.sh/api/'

  (@key) ->

  generate_url = (method, query) ->
    parsed_url = url.parse base_url
    parsed_url.query = query
    parsed_url.pathname += method
    url.format parsed_url

  generate_method = (method) ->
    (args, callback) ->
      @send method, args, callback

  send: (method, query, callback) ->
    if typeof query is 'function'
      callback = query
      query = {}
    query ?= {}
    callback ?= ->
    query.k = @key
    request generate_url(method, query), (err, res, data) ->
      return callback err if err
      try
        data = JSON.parse data
      catch error
        throw new Error "Invalid osu! API key. Please edit it in config.ls"
      callback err, data

  get_beatmaps: generate_method 'get_beatmaps'
  get_user: generate_method 'get_user'
  get_scores: generate_method 'get_scores'
  get_user_best: generate_method 'get_user_best'
  get_user_recent: generate_method 'get_user_recent'
  get_match: generate_method 'get_match'

module.exports = Osu
